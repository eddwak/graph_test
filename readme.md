# Meeting Invite Application

This test's purpose was to create a solution to invite 3 internal users in an organization by adding a request to their calendars  to a meeting called **Annual Strategic Planning Retreat – 2018** with 4 external partners.

## Technologies Used

The application has been created with PHP [Laravel 5.4.](https://laravel.com/docs/5.4) and the front- end has been built with Bootstrap 4.

The prerequisites for running the Laravel 5.4 application are [PHP 5.6](http://php.net/downloads.php) and [Composer](https://getcomposer.org/)

## Setup Laravel

To set up the application called &quot;graph\_test&quot; on Laravel version 5.4, navigate to the project directory and run the command:

    composer create-project --prefer-dist laravel/laravel graph\_test "5.4.*"

The laravel deployment server is started by running the command:

    php artisan serve

By default, the Laravel application runs on localhost:8000, therefore, opening the browser and navigating to [http://localhost:8000](http://localhost:8000) should give you the default Laravel page.

##Add the Settings File

**NB:** The settings .env file is excluded from this repository as it contains the application ID and password which for security reasons can't be uploaded to a public repository. 
Download it from the email sent and place it in the root of the application.

## Add Dependencies

We will require the following packages to complete the assignment:

- oauth2 -client for handling sign-in and OAuth token flows.
- microsoft-graph - for making calls to Microsoft Graph.
- Guzzler- for making it easy to send http requests and integrating with web services from within a php application.

To install the first 2 libraries run the command:

    composer require league/oauth2-client:dev-master microsoft/microsoft-graph

To add Guzzle to the project run the command:

    composer require guzzlehttp/guzzle

This command runs the latest Guzzle package which at the time of developing this application was Version 6.3.

## Register application

Create a new Azure AD application registration on the [Application Registration Portal](https://apps.dev.microsoft.com/)

Steps for this stage are:

Login in to the registration portal as the admin user ([admin@M365B815254.onmicrosoft.com](mailto:admin@M365B815254.onmicrosoft.com))

Select "Adding an app" on the Converged Apps List

Create an application named Graph Test

Let the application generate an application ID and password.

Add a web platform  and include the link [http://localhost:8000/callback](http://localhost:8000/callback) for the redirects after successful authentication of the app.

## Adding required permissions

 Additional permissions will be required to be able to read and write to the calendar to create the meetings and invite participants so scroll down to the section "Microsoft Graph Permissions" and click on "Add application permissions". Look for the Calendars.ReadWrite permission and add.

You have to consent as the admin for the permission to work.

## Set authentication on the app

Add the required OAuth code to the .env settings file at the root of the application and use the Application ID, Password and redirect URL created when registering the application:

     OAUTH\_APP\_ID= (App ID)
     OAUTH\_APP\_PASSWORD= (App Password)
     OAUTH\_REDIRECT\_URI=http://localhost:8000/callback
     OAUTH\_SCOPES='openid profile offline\_access user.read calendars.read calendars.readwrite'
     OAUTH\_AUTHORITY=https://login.microsoftonline.com/common
     OAUTH\_AUTHORIZE\_ENDPOINT=/oauth2/v2.0/authorize
     OAUTH\_TOKEN\_ENDPOINT=/oauth2/v2.0/token

## Add layout and structure of the app

Create the following controllers in ./app/Http/Controllers:

- AuthController.php – This controller handles the sign in to Azure AD and gets the authenticated user&#39;s profile
- InviteController.php – This controller gets the JSON string and posts it to the calendar to create the meeting and invite users.

NB: The JSON string is currently hard-coded in the controller for demo purposes. In a real-world application, the string is got from inputs e.g. form inputs or got from the AD through the Graph Endpoint.  The string that posts the meeting and invites is:

```JSON
{
  "subject": "Annual Strategic Planning Retreat - 2018",
  "body": {
    "contentType": "HTML",
    "content": "Please respond to the invitation for the annual strategic retreat."
  },
  "start": {
      "dateTime": "2018-11-20T08:00:00",
      "timeZone": "Africa/Nairobi"
  },
  "end": {
      "dateTime": "2018-11-22T17:00:00",
      "timeZone": "Africa/Nairobi"
  },  
  "attendees": [
    {
      "emailAddress": {
        "address":"AdeleV@M365B815254.OnMicrosoft.com",
        "name": "Adele Vance"
      },
      "type": "required"
    },
	 {
      "emailAddress": {
        "address":"AlexW@M365B815254.OnMicrosoft.com",
        "name": "Alex Wilber"
      },
      "type": "required"
    },
	 {
      "emailAddress": {
        "address":"AllanD@M365B815254.OnMicrosoft.com",
        "name": "Allan Deyoung"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-*****@microsoft.com",
        "name": "Anne-Marie Sylvester"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-******@microsoft.com",
        "name": "Charles Wahome"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-*****@microsoft.com",
        "name": "Marvin Ochieng"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-*****@microsoft.com",
        "name": "Duncan Okwako"
      },
      "type": "required"
    }	 
  ]
}

```

Create the following views in ./resources/views:

- layout.blade.php- The application's template
- startinvite.blade.php - the page where the user is prompted to send the meeting invite
- success.blade.php – The page where the user is redirected after the invitation is successfully created.

Update the following files:

- public/css/app.css- Remove the markup as the template is using stylesheets and scripts hosted on the cloud.
- resources/views/welcome.blade.php – Update it to direct the signed in user to schedule a meeting
- HomeController.php – add the views for creating the invites

Add the following routes:

•	Route::get('/', 'HomeController@welcome');- 
•	Route::get('/callback', 'AuthController@callback');- Callback after the Azure AD authentication
•	Route::get('/signin', 'AuthController@signin'); - Signing in
•	Route::get('/signout', 'AuthController@signout'); - Signing out
•	Route::get('/startinvite', 'HomeController@startinvite'); - The page where the user is prompted to send the meeting invite
•	Route::get('/invite', 'InviteController@invite'); - The controller that sends the meeting invites
•	Route::get('/success', 'HomeController@success'); - the page displayed to the user after a successful redirect

## Testing the application

Start the application using php artisan serve and navigating to [http://localhost:8000](http://localhost:8000)

Click on the sign in button on the welcome page, or the navigation bar link on the top right to sign in to Azure AD

Click on the Invite Participant link then the button on the page to send the invites

