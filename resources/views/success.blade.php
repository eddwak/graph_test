@extends('layout')

@section('content')
<div class="jumbotron">
  <h1>Invite successful</h1>
  <p class="lead">The meeting has been successfully scheduled and the requests sent.</p>  
</div>
@endsection