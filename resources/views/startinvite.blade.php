@extends('layout')

@section('content')
<div class="jumbotron">
  <h1>Annual Strategic Planning Retreat Invite</h1>
  <p class="lead">Schedule the retreat and invite participants</p>
  @if(isset($userName))
    <a href="/invite" class="btn btn-primary btn-large">Click here to create the meeting</a>
    @else
    <a href="/signin" class="btn btn-primary btn-large">Click here to sign in</a>
  @endif
 
</div>
@endsection