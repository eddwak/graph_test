@extends('layout')

@section('content')
<div class="jumbotron"> 
  @if(isset($userName))
    <h4>Welcome {{ $userName }}!</h4>    
  @else
    <a href="/signin" class="btn btn-primary btn-large">Click here to sign in</a>
  @endif
  <p class="lead">Click on the "Invite Participants" menu to schedule a meeting and invite participants</p>
 
</div>
@endsection