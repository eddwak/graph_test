<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
  public function welcome()
  {
    $viewData = $this->loadViewData();

    return view('welcome', $viewData);
  }
  public function startinvite()
  {
    $viewData = $this->loadViewData();

    return view('startinvite', $viewData);
  }
  public function successful()
  {
    $viewData = $this->loadViewData();

    return view('success', $viewData);
  }
}