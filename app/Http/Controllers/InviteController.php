<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class InviteController extends Controller
{
  public function invite()
  {
    $viewData = $this->loadViewData();

    // Get the access token from the cache
    $tokenCache = new TokenCache();
    $accessToken = $tokenCache->getAccessToken();

    // Create a Graph client
    $graph = new Graph();
    $graph->setAccessToken($accessToken);
  
 // Start a new Guzzle client
$client = new \GuzzleHttp\Client();
 
// Set up headers
$headers = [
  'Accept' => 'application/json',
  'Authorization' => 'Bearer ' . $accessToken,
  'Content-Type' => 'application/json',    
];

//Get JSON String to post to the Graph endpoint
//In a real world app the users and messages should be passed through form inputs and not hard coded into the json string
$json_string = '{
  "subject": "Annual Strategic Planning Retreat - 2018",
  "body": {
    "contentType": "HTML",
    "content": "Please respond to the invitation for the annual strategic retreat."
  },
  "start": {
      "dateTime": "2018-11-20T08:00:00",
      "timeZone": "Africa/Nairobi"
  },
  "end": {
      "dateTime": "2018-11-22T17:00:00",
      "timeZone": "Africa/Nairobi"
  },  
  "attendees": [
    {
      "emailAddress": {
        "address":"AdeleV@M365B815254.OnMicrosoft.com",
        "name": "Adele Vance"
      },
      "type": "required"
    },
	 {
      "emailAddress": {
        "address":"AlexW@M365B815254.OnMicrosoft.com",
        "name": "Alex Wilber"
      },
      "type": "required"
    },
	 {
      "emailAddress": {
        "address":"AllanD@M365B815254.OnMicrosoft.com",
        "name": "Allan Deyoung"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-ansil@microsoft.com",
        "name": "Anne-Marie Sylvester"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-chigita@microsoft.com",
        "name": "Charles Wahome"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-edmarv@microsoft.com",
        "name": "Marvin Ochieng"
      },
      "type": "required"
    },
    {
      "emailAddress": {
        "address":"v-duokwa@microsoft.com",
        "name": "Duncan Okwako"
      },
      "type": "required"
    }
  ]
}' ;
   

//Convert JSON String to PhP array for Guzzle to process it

$json_array = json_decode($json_string, TRUE);

 
try {
 
	// Set up the post request to the API
	$response = $client->request(
		'POST',
		'https://graph.microsoft.com/v1.0/me/events',
		array( 'headers' => $headers,'json'=>$json_array )
	);
 
  //Redirect to the successful confirmation page if the event has been posted
    return view('success');
 
// Decode the exceptions
} catch (GuzzleHttp\Exception\ClientException $e) {
    $response = $e->getResponse();
    $responseBodyAsString = $response->getBody()->getContents();
    echo $responseBodyAsString;
    exit();
}

  }
}